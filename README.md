# Read Me First

1. Java 10
2. Spring Boot
3. MySQL database

### Problem:

_Manage voting sessions_

The Spring Boot API should:
1. Register a new voting agenda (voting-agenda-controller POST /agendas/create)
2. Open a new voting session on an agenda and the session must be opened for a specified time or 1 minute by default. (session-controller POST /sessions/open/{agendaId})
3. Receive votes from associate in a sessionID, votes are YES/NO (session-controller POST /sessions/{sessionId}/vote)
4. Each member is identified by a unique id and can vote only once. 
5. Count the votes and provide the result (session-controller GET /sessions/{sessionId}/vote/results)

Other APIs:
* Find by Agenda information (voting-agenda-controller GET /agendas/{agendaId})
* Register of an associate (associate-controller POST /associates/register)

#### Data model created:
The tables are:

1. **sessionvoting.agenda**: contains agenda information such as id, creation date, description and title.
2. **sessionvoting.associated**: it contains information about the registered associate, identified by a generated id, cpf that must be unique, registration date and name.
3. **sessionvoting.voting_session**: relates the open session to the respective agendaId. It also contains the start time and the end time and for auditing purposes who opened the session (identifier)
4. **sessionvoting.associated_votexref**: thinking of a normal voting system, where the person who voted should not be identified, this table was created to record the session, associatedId, date and your vote (NO = 1, YES = 0)

Instructions to build the project:

1. "mvn clean install" to build
2. Run "VotingSessionsApplication" class or create a configuration with VM Options "-Dspring.profiles.active=native". 

##### MySQL
1. Download and install MySQL
2. The schema "sessionvoting" was manually created 

##### Start Kafka 
* Notes: Implementation related to Kafka has not been finished. Therefore, it's not necessary to start and download kafka for this system. 
1. zookeeper-server-start.bat ..\..\config\zookeeper.properties
2. kafka-server-start.bat ..\..\config\server.properties

## Swagger
To access Swagger application, before you need deploy and start the application.
http://localhost:8080/swagger-ui.html

#### challenges during implementation
1. java.sql.SQLException: The server time zone value 'Hora oficial do Brasil' is unrecognized or represents more than one time zone. You must configure either the server or JDBC driver (via the 'serverTimezone' configuration property) to use a more specifc time zone value if you want to utilize time zone support.

solved by: add to url database the sufix useTimezone=true&serverTimezone=UTC

