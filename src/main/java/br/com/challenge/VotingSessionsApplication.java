package br.com.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VotingSessionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(VotingSessionsApplication.class, args);
	}
}
