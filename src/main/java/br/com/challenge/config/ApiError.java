package br.com.challenge.config;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
public class ApiError {
    private HttpStatus status;
    private String message;
    private List<String> errors = new ArrayList<>();

    public ApiError(HttpStatus status, String message){
        this.status = status;
        this.message = message;
    }

    public ApiError(HttpStatus status, Exception ex) {
        this(status, ex.getLocalizedMessage());
        Throwable t = ex.getCause();
        while (Objects.nonNull(t)){
            errors.add(t.getMessage());
            t = t.getCause();
        }
    }
}
