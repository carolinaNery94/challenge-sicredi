package br.com.challenge.controller;

import br.com.challenge.exception.ExistingAssociatedException;
import br.com.challenge.projection.AssociatedResponse;
import br.com.challenge.service.AssociateBuilder;
import br.com.challenge.service.AssociateService;
import br.com.challenge.service.request.AssociateRegisterRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Controller
@RestController
@RequestMapping("/associates")
@Api(value = "Associate API")
public class AssociateController {

    @Autowired
    AssociateService associateService;

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(path = "/register")
    @ApiOperation(value = "Register of an associate")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully registered.", response = AssociatedResponse.class),
            @ApiResponse(code = 400, message = "Bad Request. Please check response body for further details.")
    })
    public AssociatedResponse registerAssociate(
            @ApiParam(value = "Name", required = true) @RequestHeader(value = "Name") String name,
            @ApiParam(value = "Associated data to be inserted", required = true) @RequestBody @Valid AssociateRegisterRequest registerRequest) throws ExistingAssociatedException {
        return new SpelAwareProxyProjectionFactory().createProjection(AssociatedResponse.class,
                associateService.registerAssociate(AssociateBuilder.builder()
                        .name(name)
                        .associateRegister(registerRequest)
                        .build()));
    }
}
