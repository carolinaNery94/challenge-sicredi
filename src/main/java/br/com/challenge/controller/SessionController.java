package br.com.challenge.controller;

import br.com.challenge.db.data.VotingOptionsEnum;
import br.com.challenge.exception.AssociatedAlreadyVotedException;
import br.com.challenge.filter.SessionDurationFilter;
import br.com.challenge.filter.SessionRequestFilter;
import br.com.challenge.projection.AssociatedVoteResponse;
import br.com.challenge.projection.CountSessionVotesResponse;
import br.com.challenge.projection.SessionResponse;
import br.com.challenge.projection.SessionVotesRespose;
import br.com.challenge.service.AssociateVoteBuilder;
import br.com.challenge.service.VotingSessionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;


@RestController
@Controller
@RequestMapping("/sessions")
@Api(value = "SessionResponse Voting API")
public class SessionController {

    @Autowired
    VotingSessionService votingSessionService;

    @PostMapping(path = "/open/{agendaId}")
    @ApiOperation(value = "Open a session based on the Voting Agenda ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully opened the sessionId."),
            @ApiResponse(code = 400, message = "Bad Request. Please check response body for further details."),
            @ApiResponse(code = 404, message = "SessionResponse not found.")
    })
    public SessionResponse openSession(
            @ApiParam(value = "Identification of who opened the session", required = true) @RequestHeader(value = "Identifier") String identity,
            @ApiParam(value = "Agenda Unique identifier (id)", required = true) @PathVariable("agendaId") Long agendaId,
            @ApiParam(value = "Duration in seconds") @Positive @RequestHeader(value = "Duration", required = false) Long duration) throws EntityNotFoundException {
        SpelAwareProxyProjectionFactory spelAwareProxyProjectionFactory = new SpelAwareProxyProjectionFactory();
        SessionRequestFilter sessionRequest = SessionRequestFilter.builder()
                .identity(identity)
                .agendaId(agendaId)
                .durationFilter(
                        SessionDurationFilter.builder()
                                .duration(duration)
                                .build()
                ).build();

        return spelAwareProxyProjectionFactory.createProjection(SessionResponse.class,
                votingSessionService.open(sessionRequest));

    }

    @PostMapping(path = "/{sessionId}/vote")
    @ApiOperation(value = "Vote of an associate")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully Voted."),
            @ApiResponse(code = 400, message = "Bad Request. Please check response body for further details."),
            @ApiResponse(code = 404, message = "Associated not found.")
    })
    public AssociatedVoteResponse registerVote(
            @ApiParam(value = "Session unique identifier (id)", required = true) @PathVariable("sessionId") Long sessionId,
            @ApiParam(value = "Associate unique identifier", required = true) @RequestBody @Valid Long associateId,
            @ApiParam(value = "Vote", required = true) @NotNull @RequestParam(value = "Vote of Associated") VotingOptionsEnum vote) throws AssociatedAlreadyVotedException {
        return new SpelAwareProxyProjectionFactory().createProjection(AssociatedVoteResponse.class,
                votingSessionService.registerAssociateVote(AssociateVoteBuilder.builder()
                        .sessionId(sessionId)
                        .associateId(associateId)
                        .vote(vote).build()));
    }

    @GetMapping(path = "/{sessionId}/vote/results")
    @ApiOperation(value = "Result of the session and number of votes.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved session information."),
            @ApiResponse(code = 400, message = "Bad Request. Please check response body for further details."),
            @ApiResponse(code = 404, message = "Session not found.")
    })
    public ResponseEntity<SessionVotesRespose> getResult(
            @ApiParam(value = "Session unique identifier (id)", required = true) @PathVariable(name = "sessionId") Long sessionId) {
        return ResponseEntity.ok(new SpelAwareProxyProjectionFactory().createProjection(
                SessionVotesRespose.class,
                votingSessionService.getSessionResult(sessionId)));
    }
}
