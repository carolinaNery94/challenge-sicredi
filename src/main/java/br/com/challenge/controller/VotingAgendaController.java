package br.com.challenge.controller;

import br.com.challenge.projection.VotingAgendaResponse;
import br.com.challenge.service.VotingAgendaBuilder;
import br.com.challenge.service.VotingAgendaService;
import br.com.challenge.service.request.VotingSaveRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@Controller
@RestController
@RequestMapping("/agendas")
@Api(value = "Voting Agenda API")
public class VotingAgendaController {

    @Autowired
    VotingAgendaService votingAgendaService;

    @PostMapping(path = "/create")
    @ApiOperation(value = "Creation of the Voting Agenda")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created."),
            @ApiResponse(code = 400, message = "Bad Request. Please check response body for further details.")
    })
    public VotingAgendaResponse creatingVotingAgenda(
            @ApiParam(value = "Agenda Title", required = true) @RequestParam(value = "Title") String title,
            @ApiParam(value = "Voting Agenda to be inserted", required = true) @RequestBody @Valid VotingSaveRequest request) {

        return new SpelAwareProxyProjectionFactory().createProjection(VotingAgendaResponse.class,
                votingAgendaService.createVotingAgenda(VotingAgendaBuilder.builder()
                        .title(title)
                        .votingRequest(request)
                        .build()
                )
        );
    }

    @GetMapping(path = "/{agendaId}")
    @ApiOperation(value = "Get Voting Information")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved voting information."),
            @ApiResponse(code = 400, message = "Bad Request. Please check response body for further details."),
            @ApiResponse(code = 404, message = "Voting not found.")
    })
    public ResponseEntity<VotingAgendaResponse> getVotingById(
            @ApiParam(value = "Agenda unique identifier (id)", required = true) @PathVariable("agendaId") Long agendaId) throws EntityNotFoundException {
        return ResponseEntity.ok(new SpelAwareProxyProjectionFactory().createProjection(VotingAgendaResponse.class, votingAgendaService.getAgendaById(agendaId)));
    }

}
