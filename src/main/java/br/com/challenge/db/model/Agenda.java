package br.com.challenge.db.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@Entity
@Table(name = "Agenda")
public class Agenda {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "VOTING_ID")
    private Long id;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "DESCRIPTION")
    private String description;

    @Basic
    @CreationTimestamp
    @Column(name = "CREATION_DATE")
    private LocalDate creationDate;

    public Agenda(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Agenda(Long id, String title, String description){
        this(title, description);
        this.id = id;
        this.creationDate = LocalDate.now();
    }
}
