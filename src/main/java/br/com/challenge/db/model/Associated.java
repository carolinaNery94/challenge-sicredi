package br.com.challenge.db.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Associated")
public class Associated {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ASSOCIATED_ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CPF")
    private String cpf;

    @Basic
    @CreationTimestamp
    @Column(name = "DATE")
    private LocalDate date;

    @OneToMany
    @LazyCollection(LazyCollectionOption.TRUE)
    private Set<AssociatedVote> associatedVotes;

    public Associated(String name, String cpf, String vote, Long sessionId, LocalDate date) {
        this.name = name;
        this.cpf = cpf;
        this.date = date;
    }

    public Associated(String name, String cpf){
        new Associated(name, cpf, null, null, null);
    }
}
