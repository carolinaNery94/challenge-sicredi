package br.com.challenge.db.model;

import br.com.challenge.db.data.VotingOptionsEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@IdClass(AssociatedVoteID.class)
@Table(name = "AssociatedVoteXREF")
public class AssociatedVote implements Serializable {

    @Id
    @Column(name = "ASSOCIATED_ID")
    private Long associatedId;

    @Id
    @Column(name = "SESSION_ID")
    private Long sessionId;

    @Column(name = "VOTE")
    private VotingOptionsEnum vote;

    @Basic
    @CreationTimestamp
    @Column(name = "DATE")
    private LocalDate date;
}
