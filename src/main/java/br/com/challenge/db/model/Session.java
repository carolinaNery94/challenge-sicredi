package br.com.challenge.db.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "VotingSession")
public class Session {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "SESSION_ID")
    private Long id;

    @Column(name = "IDENTIFIER")
    private String identifier;

    @Column(name = "VOTING_AGENDA_ID")
    private Long votingAgendaId;

    @Column(name = "START")
    private LocalDateTime start;

    @Column(name = "END")
    private LocalDateTime end;

    public Session(String identifier, Long votingAgendaId, LocalDateTime start, LocalDateTime end) {
        this.identifier = identifier;
        this.votingAgendaId = votingAgendaId;
        this.start = start;
        this.end = end;
    }

}
