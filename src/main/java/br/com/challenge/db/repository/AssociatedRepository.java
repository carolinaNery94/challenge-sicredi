package br.com.challenge.db.repository;

import br.com.challenge.db.model.Associated;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Optional;

public interface AssociatedRepository extends CrudRepository<Associated, Long> {

    @Query("SELECT DISTINCT NEW java.lang.Boolean(true) FROM Associated c WHERE CPF = :#{#cpf}")
    Optional<Boolean> findBycpf(@Param("cpf") String cpf);

    @Modifying
    @Query(value = "INSERT INTO Associated(ASSOCIATED_ID, NAME, CPF, VOTE, SESSION_ID, DATE) " +
            "VALUES (?1, ?2, ?3, ?4, ?5, ?6)", nativeQuery = true)
    void saveVote(Long id, String name, String cpf, String vote, Long sessionId, LocalDate date);
}
