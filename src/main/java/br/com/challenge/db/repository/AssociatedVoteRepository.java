package br.com.challenge.db.repository;

import br.com.challenge.db.data.VotingOptionsEnum;
import br.com.challenge.db.model.AssociatedVote;
import br.com.challenge.db.model.AssociatedVoteID;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AssociatedVoteRepository extends CrudRepository<AssociatedVote, AssociatedVoteID> {

    @Query("SELECT distinct NEW java.lang.Boolean(true) " +
            "FROM AssociatedVote av " +
            "WHERE av.associatedId = :#{#associatedId} " +
            "AND av.sessionId = :#{#sessionId}")
    Optional<Boolean> associatedAlreadyVotedForTheSession(@Param("associatedId") Long associatedId, @Param("sessionId") Long sessionId);

    @Query("SELECT count(av) from AssociatedVote av " +
            "WHERE av.sessionId = :#{#sessionId} " +
            "AND av.vote = :#{#voteType} ")
    Integer countByVoteTypeAndSessionId(@Param("sessionId") Long sessionId, @Param("voteType") VotingOptionsEnum voteType);
}
