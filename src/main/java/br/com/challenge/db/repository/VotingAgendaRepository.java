package br.com.challenge.db.repository;

import br.com.challenge.db.model.Agenda;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface VotingAgendaRepository extends CrudRepository<Agenda, Long> {

    @Query("SELECT V FROM Agenda V WHERE VOTING_ID = :#{#votingId}")
    Agenda find(@Param("votingId") Long votingId);
}
