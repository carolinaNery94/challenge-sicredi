package br.com.challenge.db.repository;

import br.com.challenge.db.model.Session;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface VotingSessionRepository extends CrudRepository<Session, Long> {

    @Query("SELECT s.end FROM Session s WHERE SESSION_ID = :#{#sessionId}")
    LocalDateTime collectExpirationDate(@Param("sessionId") Long sessionId);
}
