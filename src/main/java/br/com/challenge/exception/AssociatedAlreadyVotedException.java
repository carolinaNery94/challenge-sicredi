package br.com.challenge.exception;

public class AssociatedAlreadyVotedException extends Exception {
    public AssociatedAlreadyVotedException(Long associatedId, Long sessionId) {
        super(String.format("Associate %s already vote in this session [%s]", associatedId, sessionId));
    }
}
