package br.com.challenge.exception;

public class ExistingAssociatedException extends Exception{
    public ExistingAssociatedException(){
        super("CPF already registered!");
    }
}
