package br.com.challenge.exception;

public class SessionClosedException extends RuntimeException {

    public SessionClosedException(final Long sessionId){
        super(String.format("The session [%s] is closed, please start another", sessionId));
    }
}
