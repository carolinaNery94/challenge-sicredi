package br.com.challenge.filter;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@Builder
public class SessionDurationFilter {
    private Long duration;

    public boolean hasDuration() {
        return Objects.nonNull(duration);
    }
}
