package br.com.challenge.filter;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class SessionRequestFilter implements Serializable {
    private String identity;
    private Long agendaId;
    private SessionDurationFilter durationFilter;
}
