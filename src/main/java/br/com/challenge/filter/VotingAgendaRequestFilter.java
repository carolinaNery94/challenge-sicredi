package br.com.challenge.filter;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class VotingAgendaRequestFilter implements Serializable {
    private String title;
    private String description;
}
