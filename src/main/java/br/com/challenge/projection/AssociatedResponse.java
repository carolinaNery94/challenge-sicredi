package br.com.challenge.projection;

import java.time.LocalDate;

public interface AssociatedResponse {
    Long getId();
    String getName();
    String getCpf();
    LocalDate getDate();
}
