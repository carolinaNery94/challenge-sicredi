package br.com.challenge.projection;

import br.com.challenge.db.data.VotingOptionsEnum;

import java.time.LocalDate;

public interface AssociatedVoteResponse {
    Long getAssociatedId();
    Long getSessionId();
    VotingOptionsEnum getVote();
    LocalDate getDate();
}
