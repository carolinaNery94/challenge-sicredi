package br.com.challenge.projection;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Builder
@Getter
@EqualsAndHashCode
public class CountSessionVotesResponse {
    private Integer votesYes;
    private Integer votesNo;
}
