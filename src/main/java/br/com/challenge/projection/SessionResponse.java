package br.com.challenge.projection;

import java.time.LocalDateTime;

public interface SessionResponse {
    Long getId();
    String getIdentifier();
    Long getVotingAgendaId();
    LocalDateTime getStart();
    LocalDateTime getEnd();
}
