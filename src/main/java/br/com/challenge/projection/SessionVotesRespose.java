package br.com.challenge.projection;

public interface SessionVotesRespose {
    Integer getVotesYes();
    Integer getVotesNo();
}
