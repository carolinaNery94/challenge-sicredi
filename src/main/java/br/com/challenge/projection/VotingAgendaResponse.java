package br.com.challenge.projection;

import java.time.LocalDate;

public interface VotingAgendaResponse {
    Long getId();
    String getTitle();
    String getDescription();
    LocalDate getCreationDate();
}
