package br.com.challenge.service;

import java.time.LocalDateTime;
import java.util.Objects;

abstract class AbstractService {
    private static final long MINUTE_SCALE = 60L * 1000000000;

    long getDurationTime(Long duration){
        final long time;
        if (Objects.nonNull(duration)){
            time = duration * 1000000000;
        } else {
            time = MINUTE_SCALE;
        }
        return time;
    }

    LocalDateTime getExpirationTime(LocalDateTime start, long duration){
        return start.plusNanos(duration);
    }
}
