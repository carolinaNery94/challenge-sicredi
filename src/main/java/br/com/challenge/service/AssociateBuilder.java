package br.com.challenge.service;

import br.com.challenge.service.request.AssociateRegister;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AssociateBuilder {
    private String name;
    private AssociateRegister associateRegister;
}
