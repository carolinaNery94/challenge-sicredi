package br.com.challenge.service;

import br.com.challenge.db.model.Associated;
import br.com.challenge.db.repository.AssociatedRepository;
import br.com.challenge.exception.ExistingAssociatedException;
import br.com.challenge.service.request.AssociateRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AssociateService {

    @Autowired
    private AssociatedRepository associatedRepository;

    public Associated registerAssociate(AssociateBuilder associateBuilder) throws ExistingAssociatedException {
        final AssociateRegister associateRegister = associateBuilder.getAssociateRegister();
        final String name = associateBuilder.getName();

        final String cpf = associateRegister.getAssociate(name).getCpf();
        this.validateCpf(cpf);

        final Associated associated = associateRegister.getAssociate(name);

        return save(associated);
    }

    public Associated save(Associated associated) {
        return associatedRepository.save(associated);
    }

    private void validateCpf(String cpf) throws ExistingAssociatedException {
        final Boolean cpfExistis = associatedRepository.findBycpf(cpf).orElse(false);
        if (cpfExistis){
            throw new ExistingAssociatedException();
        }

    }
}
