package br.com.challenge.service;

import br.com.challenge.db.data.VotingOptionsEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class AssociateVoteBuilder {
    private Long sessionId;
    private Long associateId;
    private VotingOptionsEnum vote;
}
