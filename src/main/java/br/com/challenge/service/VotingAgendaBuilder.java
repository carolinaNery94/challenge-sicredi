package br.com.challenge.service;

import br.com.challenge.service.request.VotingRequest;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class VotingAgendaBuilder {
    private String title;
    private VotingRequest votingRequest;
}
