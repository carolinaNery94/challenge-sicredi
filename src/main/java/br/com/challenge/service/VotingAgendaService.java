package br.com.challenge.service;

import br.com.challenge.db.model.Agenda;
import br.com.challenge.db.repository.VotingAgendaRepository;
import br.com.challenge.service.request.VotingRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class VotingAgendaService {

    @Autowired
    private VotingAgendaRepository votingAgendaRepository;

    public Agenda createVotingAgenda(VotingAgendaBuilder votingAgenda){
        final VotingRequest votingRequest = votingAgenda.getVotingRequest();
        final String title = votingAgenda.getTitle();
        final Agenda voting = votingRequest.getVoting(title);

        return save(voting);
    }

    public Agenda save(Agenda agenda) {
        return votingAgendaRepository.save(agenda);
    }

    public Agenda getAgendaById(Long id) throws EntityNotFoundException {
        return votingAgendaRepository.find(id);
    }
}
