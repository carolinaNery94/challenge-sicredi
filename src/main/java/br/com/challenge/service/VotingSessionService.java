package br.com.challenge.service;

import br.com.challenge.db.data.VotingOptionsEnum;
import br.com.challenge.db.model.AssociatedVote;
import br.com.challenge.db.model.Session;
import br.com.challenge.db.repository.AssociatedRepository;
import br.com.challenge.db.repository.AssociatedVoteRepository;
import br.com.challenge.db.repository.VotingAgendaRepository;
import br.com.challenge.db.repository.VotingSessionRepository;
import br.com.challenge.exception.AssociatedAlreadyVotedException;
import br.com.challenge.exception.SessionClosedException;
import br.com.challenge.filter.SessionRequestFilter;
import br.com.challenge.projection.CountSessionVotesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
public class VotingSessionService extends AbstractService{

    @Autowired
    private VotingSessionRepository votingSessionRepository;

    @Autowired
    private VotingAgendaRepository votingAgendaRepository;

    @Autowired
    private AssociatedVoteRepository associatedVoteRepository;

    @Autowired
    private AssociatedRepository associatedRepository;

    public Session open(SessionRequestFilter sessionRequest) throws EntityNotFoundException {
        final String identity = sessionRequest.getIdentity();
        final Long agendaId = sessionRequest.getAgendaId();
        final Long duration = sessionRequest.getDurationFilter().getDuration();

        votingAgendaRepository.findById(agendaId).orElseThrow(EntityNotFoundException::new);

        final long sessionDuration = getDurationTime(duration);
        final LocalDateTime expiration = getExpirationTime(LocalDateTime.now(), sessionDuration);

        final Session session = new Session(identity, agendaId, LocalDateTime.now(), expiration);

        return votingSessionRepository.save(session);

    }

    public AssociatedVote registerAssociateVote(AssociateVoteBuilder associateVoteBuilder) throws EntityNotFoundException, AssociatedAlreadyVotedException {
        votingSessionRepository.findById(associateVoteBuilder.getSessionId()).orElseThrow(EntityNotFoundException::new);

        final Long sessionId = associateVoteBuilder.getSessionId();
        final Long associateId = associateVoteBuilder.getAssociateId();

        if (this.validateIfSessionIsExpired(sessionId) == false) {
            throw new SessionClosedException(sessionId);
        }

        associatedRepository.findById(associateId).orElseThrow(EntityNotFoundException::new);

        this.validateIfAssociateCanVote(associateId, sessionId);
        return this.saveAssociatedVote(associateId, sessionId, associateVoteBuilder.getVote());

    }

    private boolean validateIfSessionIsExpired(Long sessionId) {
        LocalDateTime session_endDate = votingSessionRepository.collectExpirationDate(sessionId);
        return LocalDateTime.now().isBefore(session_endDate);
    }

    private void validateIfAssociateCanVote(Long associateId, Long sessionId) throws AssociatedAlreadyVotedException {
        final Boolean hasVote = associatedVoteRepository.associatedAlreadyVotedForTheSession(associateId, sessionId).orElse(false);
        if (hasVote) {
            throw new AssociatedAlreadyVotedException(associateId, sessionId);
        }
    }

    private AssociatedVote saveAssociatedVote(Long associatedId, Long sessionId, VotingOptionsEnum vote) {
        AssociatedVote associatedVote = new AssociatedVote();
        associatedVote.setSessionId(sessionId);
        associatedVote.setAssociatedId(associatedId);
        associatedVote.setDate(LocalDate.now());
        associatedVote.setVote(vote);

        return associatedVoteRepository.save(associatedVote);
    }

    public CountSessionVotesResponse getSessionResult(Long sessionId) {
        votingSessionRepository.findById(sessionId).orElseThrow(EntityNotFoundException::new);

        Integer countByNo = associatedVoteRepository.countByVoteTypeAndSessionId(sessionId, VotingOptionsEnum.NO);
        Integer countByYes = associatedVoteRepository.countByVoteTypeAndSessionId(sessionId, VotingOptionsEnum.YES);

        CountSessionVotesResponse response = CountSessionVotesResponse.builder()
                .votesNo(countByNo)
                .votesYes(countByYes)
                .build();

        return response;
    }
}
