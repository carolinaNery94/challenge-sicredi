package br.com.challenge.service.request;

import br.com.challenge.db.model.Associated;

public interface AssociateRegister {
    Associated getAssociate(String name);
}
