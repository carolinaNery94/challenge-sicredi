package br.com.challenge.service.request;

import br.com.challenge.db.model.Associated;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class AssociateRegisterRequest implements AssociateRegister {
    private String cpf;

    @Override
    public Associated getAssociate(String name) {
        Associated associated = new Associated();
        associated.setName(name);
        associated.setCpf(this.cpf);
        return associated;
    }
}
