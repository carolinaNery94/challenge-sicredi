package br.com.challenge.service.request;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class AssociateVoteRequest implements AssociateRequest {
    private String associateId;
}
