package br.com.challenge.service.request;

import br.com.challenge.db.model.Agenda;

public interface VotingRequest {
    Agenda getVoting(String title);
}
