package br.com.challenge.service.request;

import br.com.challenge.db.model.Agenda;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class VotingSaveRequest implements VotingRequest {
    private String description;

    public Agenda getVoting(String title) {
        Agenda agenda = new Agenda();
        agenda.setTitle(title);
        agenda.setDescription(this.description);
        return agenda;
    }
}
