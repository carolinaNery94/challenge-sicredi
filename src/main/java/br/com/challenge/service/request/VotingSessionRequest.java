package br.com.challenge.service.request;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class VotingSessionRequest implements SessionRequest {
    private Long voting_id;
    private Integer duration;
}
