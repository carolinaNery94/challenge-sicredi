package br.com.challenge.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("br.com.challenge.db.repository")
@EntityScan(basePackages = "br.com.challenge.db.model")
public class TestConfig {
}
