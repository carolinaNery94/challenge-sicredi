package br.com.challenge.controller.binding;

import br.com.challenge.utils.HeaderConstants;
import lombok.Data;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(MockitoJUnitRunner.class)
public abstract class BaseControllerTest {

    @Data
    protected static class BaseHeaderValues {
        private String name;
    }

    protected BaseHeaderValues baseHeader;

    @Before
    public void setUp() throws Exception {
        baseHeader = new BaseHeaderValues();
    }

    protected ResultMatcher jsonPathWithStatus(Matcher<?> matcher) {
        return jsonPath("status", matcher);
    }

    protected ResultMatcher jsonPathWithMessage(Matcher<?> matcher) {
        return jsonPath("message", matcher);
    }

    protected ResultMatcher jsonPathWithErrors(Matcher<?> matcher) {
        return jsonPath("errors", matcher);
    }


    public HttpHeaders createBaseHeader() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HeaderConstants.NAME, baseHeader.getName());

        return httpHeaders;
    }
}
