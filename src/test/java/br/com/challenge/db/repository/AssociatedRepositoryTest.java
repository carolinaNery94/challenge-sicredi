package br.com.challenge.db.repository;

import br.com.challenge.config.TestConfig;
import br.com.challenge.db.model.Associated;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.assertj.core.api.Assertions.assertThat;

@DBRider
@DataJpaTest
@RunWith(SpringRunner.class)
@DataSet(cleanBefore = true, cleanAfter = true, disableConstraints = true, value = "datasets/Associated.yml")
@ContextConfiguration(classes = TestConfig.class)
@TestPropertySource(locations = {"classpath:test.properties"})
public class AssociatedRepositoryTest {

    @Autowired
    private AssociatedRepository associatedRepository;

    @Test
    public void findBycpf_whenAssociatedIsRegistered_shoudlReturnTrue() {
        assertThat(associatedRepository.findBycpf("033"), equalTo(Optional.of(true)));
    }

    @Test
    public void save_whenAssociatedNotExistInDatabase_shouldSaveWithSuccess() {
        Associated associated = new Associated();
        associated.setName("NEW ASSOCIATED");
        associated.setCpf("08");

        associatedRepository.save(associated);
        assertThat(associatedRepository.findById(1L)).map(Associated::getCpf).contains("08");
    }
}
