package br.com.challenge.db.repository;

import br.com.challenge.config.TestConfig;
import br.com.challenge.db.data.VotingOptionsEnum;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.assertj.core.api.Assertions.assertThat;


@DBRider
@DataJpaTest
@RunWith(SpringRunner.class)
@DataSet(cleanBefore = true, cleanAfter = true, disableConstraints = true, value = "datasets/AssociatedVote.yml")
@ContextConfiguration(classes = TestConfig.class)
@TestPropertySource(locations = {"classpath:test.properties"})
public class AssociatedVoteRepositoryTest {

    @Autowired
    AssociatedVoteRepository associatedVoteRepository;

    @Test
    public void associatedAlreadyVotedForTheSession_ifAssociateAlreadyVoted_shouldReturnTrue(){
        assertThat(associatedVoteRepository.associatedAlreadyVotedForTheSession(31L, 27L), equalTo(Optional.of(true)));
    }

    @Test
    public void associatedAlreadyVotedForTheSession_ifAssociateNotVoted_shouldReturnFalse(){
        assertThat(associatedVoteRepository.associatedAlreadyVotedForTheSession(60L, 27L), equalTo(Optional.empty()));
    }

    @Test
    public void countByVoteTypeAndSessionId_shouldReturnTheCountForSessions(){
        assertThat(associatedVoteRepository.countByVoteTypeAndSessionId(27L, VotingOptionsEnum.NO)).isEqualTo(1);
    }
}
