package br.com.challenge.db.repository;

import br.com.challenge.config.TestConfig;
import br.com.challenge.db.model.Agenda;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;


@DBRider
@DataJpaTest
@RunWith(SpringRunner.class)
@DataSet(cleanBefore = true, cleanAfter = true, disableConstraints = true, value = "datasets/Agenda.yml")
@ContextConfiguration(classes = TestConfig.class)
@TestPropertySource(locations = {"classpath:test.properties"})
public class VotingAgendaRepositoryTest {

    @Autowired
    VotingAgendaRepository votingAgendaRepository;

    @Test
    public void find_whenAgendaExists_shoudlReturnAgendaInformation() {
        Agenda agenda = new Agenda();
        agenda.setId(23L);
        agenda.setCreationDate(LocalDate.parse("2020-08-23"));
        agenda.setDescription("agenda");
        agenda.setTitle("agenda1");

        assertThat(votingAgendaRepository.find(23L)).isEqualTo(agenda);
    }

    @Test
    public void findById_whenAgendaExists_shouldReturnAgenda(){
        assertThat(votingAgendaRepository.findById(25L)).map(Agenda::getId).contains(25L);
    }

}
