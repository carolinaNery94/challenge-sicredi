package br.com.challenge.db.repository;

import br.com.challenge.config.TestConfig;
import br.com.challenge.db.model.Agenda;
import br.com.challenge.db.model.Session;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;

@DBRider
@DataJpaTest
@RunWith(SpringRunner.class)
@DataSet(cleanBefore = true, cleanAfter = true, disableConstraints = true, value = "datasets/Session.yml")
@ContextConfiguration(classes = TestConfig.class)
@TestPropertySource(locations = {"classpath:test.properties"})
public class VotingSessionRepositoryTest {

    @Autowired
    VotingSessionRepository votingSessionRepository;

    @Test
    public void findById_whenSessionExists_shouldReturnSession() {
        assertThat(votingSessionRepository.findById(27L)).map(Session::getId).contains(27L);
    }

    @Test
    public void findById_whenSessionDoesNotExists_shouldReturnEmptySession() {
        assertThat(votingSessionRepository.findById(28L)).isEqualTo(Optional.empty());
    }

    @Test
    public void collectExpirationDate_shouldReturnLocalDateTime() {
        assertThat(votingSessionRepository.collectExpirationDate(27L)).isEqualTo(LocalDateTime.of(2020, Month.AUGUST, 23, 17, 03, 25));
    }
}
