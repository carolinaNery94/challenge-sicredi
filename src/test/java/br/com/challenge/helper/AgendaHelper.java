package br.com.challenge.helper;

import br.com.challenge.db.model.Agenda;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AgendaHelper {

    public static Agenda createAgenda() {
        Agenda agenda = new Agenda();
        agenda.setTitle("title");
        agenda.setDescription("description");

        return agenda;
    }
}
