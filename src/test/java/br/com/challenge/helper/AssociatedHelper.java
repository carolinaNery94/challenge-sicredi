package br.com.challenge.helper;

import br.com.challenge.db.model.Associated;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AssociatedHelper {
    public static Associated createAssociated(){
        Associated associated = new Associated();
        associated.setId(1L);
        associated.setCpf("1234");
        associated.setName("NAME");
        associated.setDate(LocalDate.now());

        return associated;
    }
}
