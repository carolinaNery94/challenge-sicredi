package br.com.challenge.helper;

import br.com.challenge.db.data.VotingOptionsEnum;
import br.com.challenge.db.model.AssociatedVote;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AssociatedVoteHelper {

    public static AssociatedVote createAssociatedVote() {
        AssociatedVote associatedVote = new AssociatedVote();
        associatedVote.setDate(LocalDate.now());
        associatedVote.setAssociatedId(1L);
        associatedVote.setSessionId(1L);
        associatedVote.setVote(VotingOptionsEnum.NO);

        return associatedVote;
    }

}
