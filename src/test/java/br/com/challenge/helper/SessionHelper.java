package br.com.challenge.helper;

import br.com.challenge.db.model.Session;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SessionHelper {

    public static Session createSession() {
        Session session = new Session();
        session.setId(1L);
        session.setIdentifier("TEST");
        session.setVotingAgendaId(1L);
        session.setEnd(LocalDateTime.now());
        session.setStart(LocalDateTime.now());

        return session;
    }
}
