package br.com.challenge.service;

import br.com.challenge.db.model.Associated;
import br.com.challenge.db.repository.AssociatedRepository;
import br.com.challenge.exception.ExistingAssociatedException;
import br.com.challenge.helper.AssociatedHelper;
import br.com.challenge.service.request.AssociateRegister;
import br.com.challenge.utils.HeaderConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class AssociateServiceTest {

    @Spy
    @InjectMocks
    private AssociateService associateService;

    @Mock
    private AssociatedRepository associatedRepository;

    private AssociateBuilder associateBuilder;

    @Mock
    private AssociateRegister associateRegister;

    @Before
    public void setup() {
        associateBuilder = AssociateBuilder.builder().name(HeaderConstants.NAME).associateRegister(associateRegister).build();
    }

    @Test
    public void registerAssociate_WHEN_happy_path_THEN_should_return_registered_associate() throws ExistingAssociatedException {
        Associated associated = AssociatedHelper.createAssociated();
        doReturn(associated).when(associatedRepository).save(any());

        assertThat(associateService.save(associated)).usingRecursiveComparison().isEqualTo(associated);
    }
}
