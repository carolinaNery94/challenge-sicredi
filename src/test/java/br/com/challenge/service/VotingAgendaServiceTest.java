package br.com.challenge.service;

import br.com.challenge.db.model.Agenda;
import br.com.challenge.db.repository.VotingAgendaRepository;
import br.com.challenge.helper.AgendaHelper;
import br.com.challenge.service.request.VotingRequest;
import br.com.challenge.utils.HeaderConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class VotingAgendaServiceTest {

    @Spy
    @InjectMocks
    private VotingAgendaService votingAgendaService;

    @Mock
    private VotingAgendaRepository votingAgendaRepository;

    private VotingAgendaBuilder votingAgendaBuilder;

    @Mock
    private VotingRequest votingRequest;


    @Before
    public void setup() {
        votingAgendaBuilder = VotingAgendaBuilder.builder().title(HeaderConstants.TITLE).votingRequest(votingRequest).build();
    }

    @Test
    public void createVotingAgenda_WHEN_happy_path_THEN_should_return_created_agenda() {
        Agenda agenda = AgendaHelper.createAgenda();
        doReturn(agenda).when(votingAgendaRepository).save(any());

        assertThat(votingAgendaService.createVotingAgenda(votingAgendaBuilder)).usingRecursiveComparison().isEqualTo(agenda);
    }
}
