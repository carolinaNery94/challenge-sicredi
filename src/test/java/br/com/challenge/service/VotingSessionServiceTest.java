package br.com.challenge.service;

import br.com.challenge.db.data.VotingOptionsEnum;
import br.com.challenge.db.model.Agenda;
import br.com.challenge.db.model.Session;
import br.com.challenge.db.repository.AssociatedRepository;
import br.com.challenge.db.repository.AssociatedVoteRepository;
import br.com.challenge.db.repository.VotingAgendaRepository;
import br.com.challenge.db.repository.VotingSessionRepository;
import br.com.challenge.filter.SessionDurationFilter;
import br.com.challenge.filter.SessionRequestFilter;
import br.com.challenge.helper.AgendaHelper;
import br.com.challenge.helper.SessionHelper;
import br.com.challenge.utils.HeaderConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class VotingSessionServiceTest {

    @Spy
    @InjectMocks
    private VotingSessionService votingSessionService;

    @Mock
    private VotingSessionRepository votingSessionRepository;

    @Mock
    private VotingAgendaRepository votingAgendaRepository;

    @Mock
    private AssociatedVoteRepository associatedVoteRepository;

    @Mock
    private AssociatedRepository associatedRepository;

    @Mock
    private SessionRequestFilter sessionRequestFilter;

    @Mock
    private SessionDurationFilter sessionDurationFilter;

    private AssociateVoteBuilder associateVoteBuilder;


    @Before
    public void setup() {
        associateVoteBuilder = AssociateVoteBuilder.builder().associateId(HeaderConstants.ASSOCIATE_ID).vote(VotingOptionsEnum.NO).sessionId(HeaderConstants.SESSION_ID).build();
        sessionRequestFilter = SessionRequestFilter.builder().durationFilter(SessionDurationFilter.builder().duration(60L).build()).agendaId(1L).identity("TEST").build();
    }

    @Test(expected = EntityNotFoundException.class)
    public void open_WHEN_happy_path_THEN_should_return_session() throws EntityNotFoundException {
        Session session = SessionHelper.createSession();
        Agenda agenda = AgendaHelper.createAgenda();

        votingAgendaRepository.findById(agenda.getId());
        assertThat(votingSessionService.open(sessionRequestFilter)).usingRecursiveComparison().isEqualTo(session);
    }

}
