package br.com.challenge.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class HeaderConstants {

    public static final String NAME = "Name";
    public static final Long ASSOCIATE_ID = 1L;
    public static final Long SESSION_ID = 1L;
    public static final String TITLE = "title";

}
